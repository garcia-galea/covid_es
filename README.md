## covid_es

Este proyecto contiene un dashboard escrito en R utilizando el paquete [flexdashboard](https://rmarkdown.rstudio.com/flexdashboard/) que realiza gráficos sobre el número de nuevos casos de COVID-19 detectatos en España a partir de los datos suministrados por las Comunidades Autónomas a la [Red de Vigilancia Epidemiológica (RENAVE)](https://cnecovid.isciii.es/). Utiliza los csv's que este organismo suministra con los casos diarios agregados por [provincia](https://cnecovid.isciii.es/covid19/resources/casos_diagnostico_provincia.csv) o [comunidad autónoma](https://cnecovid.isciii.es/covid19/resources/casos_diagnostico_ccaa.csv) de residencia.

**covid_es_db2.Rmd**: Código del dashboard. Puede verse en ejecución [aquí](https://garcia-galea.shinyapps.io/covid_es/).

**censo.csv**: Datos del censo provincial según el padrón municipal del 01/01/2019 del [INE](https://www.ine.es/jaxiT3/Tabla.htm?t=2852&L=0) utilizado para relativizar los datos a la densidad poblacional.

**data_backup.rda**: Copia de respaldo de los datos que el dashboard descarga de la RENAVE para cuando dejen de estar disponibles en la red.

.Rprofile, renv/ y renv.lock permiten reproducir en un nuevo proyecto local el entorno en que el dashboard fue programado. Consultar la documentacón del paquete [renv](https://rstudio.github.io/renv/) para más información. 